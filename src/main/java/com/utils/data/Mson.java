package com.utils.data;

import com.google.gson.Gson;

import java.lang.reflect.Type;

public class Mson {
    public static String toJson(Object obj) {
        Gson gson = new Gson();
        return gson.toJson(obj);
    }
    public static <T> T fromJson(String json, Class<T> classOfT){
        Gson gson = new Gson();
        return gson.fromJson(json, classOfT);
    }

    public static <T> T fromJson(String json, Type typeOfT){
        Gson gson = new Gson();
        return gson.fromJson(json, typeOfT);
    }
}
