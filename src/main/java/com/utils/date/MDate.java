package com.utils.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Te class {@code MDate} contains methods for performing basic
 * date operations such as add days or months quickly in a date
 * with int specific format.
 *
 * @author MD-Garrido
 */
public class MDate {
    public static final String MX_TIME_ZONE = "America/Mexico_City";

    /**
     * Constant identifying the standard int format. It only works until the year 9999.
     */
    public final static String STD_INT_PATTERN_DATE = "yyyyMMdd";

    /**
     * Don't let anyone instantiate this class.
     */
    private MDate() {}

    /**
     * Formats a Date into a date/time string.
     * @param date the time value to be formatted into a time string.
     * @param pattern the pattern describing the date and time format.
     * @return the formatted time string.
     */
    public static String dateToString(Date date, String pattern){
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        return dateFormat.format(date);
    }

    /**
     * Parsers text from the beginning of the given string to produce a date.
     * @param source A <code>String</code> whose beginning should be parsed.
     * @param pattern the pattern describing the date and time format.
     * @return A <code>Date</code> parsed from the string.
     * @throws ParseException if the beginning of the specified string
     *            cannot be parsed.
     */
    public static Date stringToDate(String source, String pattern) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        return dateFormat.parse(source);
    }

    /**
     * Formats a Date into a date int with {@value #STD_INT_PATTERN_DATE} format.
     * @param date the time value to be formatted into a date int.
     * @return the formatted
     */
    public static int dateToInt(Date date){
        return Integer.parseInt(dateToString(date, STD_INT_PATTERN_DATE));
    }

    /**
     * Parsers int date to produce a date with {@value #STD_INT_PATTERN_DATE} format.
     * @param source A <code>int</code> whose should be parsed between 19700101 and 99991231
     * @return A <code>Date</code> parsed from the int.
     * @throws IllegalArgumentException if source is out of range
     */
    public static Date intToDate(int source) {
        if(source < 19700101 || 99991231 < source){
            throw new IllegalArgumentException("Source is out of range");
        }
        try {
            return stringToDate(String.valueOf(source),STD_INT_PATTERN_DATE);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int intDate(){
        return dateToInt(new Date());
    }

    /**
     *
     * @param timestamp
     * @param months
     * @return
     */
    public static long addMonthsToTimestamp(long timestamp, int months){
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp);
        cal.add(Calendar.MONTH, months);
        return cal.getTimeInMillis();
    }

    /**
     *
     * @param date
     * @param months
     * @return
     */
    public static Date addMonthsToDate(Date date, int months){
        return new Date(addMonthsToTimestamp(date.getTime(), months));
    }

    /**
     *
     * @param date
     * @param months
     * @return
     * @throws ParseException
     */
    public static int addMonthsToIntDate(int date, int months) throws ParseException {
        return dateToInt(addMonthsToDate(intToDate(date), months));
    }

    public static int intDateAddMonths(int months) throws ParseException {
        return addMonthsToIntDate(intDate(), months);
    }


    public static long addDaysToTimestamp(long timestamp, int days){
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp);
        cal.add(Calendar.DATE, days);
        return cal.getTimeInMillis();
    }

    public static Date addDaysToDate(Date date, int days){
        return new Date(addDaysToTimestamp(date.getTime(), days));
    }

    public static int addDaysToIntDate(int date, int days) throws ParseException {
        return dateToInt(addDaysToDate(intToDate(date), days));
    }

    public static int intDateAddDays(int days) throws ParseException {
        return addDaysToIntDate(intDate(), days);
    }

    public static String dateToString(Date date, String pattern, String timeZone){
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
        return dateFormat.format(date);
    }

    public static Date stringToDate(String source, String pattern, String timeZone) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
        return dateFormat.parse(source);
    }

    public static Date nowTimeZone(String timeZone) throws ParseException {
        Date date = new Date();
        String pattern = "yyyy/MM/dd HH:mm:ss";
        return stringToDate(dateToString(date, pattern, timeZone), pattern, timeZone);
    }

    public static Date nowMX(){
        try {
            return nowTimeZone(MX_TIME_ZONE);
        }catch (Exception e){
            System.out.println(e.getLocalizedMessage());
        }
        return null;
    }

    public static int nowIntMX(){
        return dateToInt(nowMX());
    }


}
