package com.utils.date;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class MLocalDate {
    public static LocalDateTime intToDateTime(int date){
        String dateStr = String.valueOf(date);
        return LocalDateTime.of(
                Integer.parseInt(dateStr.substring(0,4)),
                Integer.parseInt(dateStr.substring(4,6)),
                Integer.parseInt(dateStr.substring(6,8)),0,0);
    }

    public static int dateTimeToInt(LocalDateTime localDateTime){
        return Integer.parseInt(String.valueOf(localDateTime.getYear()) + String.format("%02d", localDateTime.getMonthValue()) +
                String.format("%02d", localDateTime.getDayOfMonth()));
    }

    public static LocalDateTime now(){
        return LocalDateTime.now();
    }

    public static LocalDateTime now(int timeZone){
        return LocalDateTime.now(ZoneOffset.ofHours(timeZone));
    }

    public static int nowInt(){
        return dateTimeToInt(now());
    }

    public static int nowInt(int timeZone){
        return dateTimeToInt(now(timeZone));
    }
}
