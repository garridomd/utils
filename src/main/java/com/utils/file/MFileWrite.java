package com.utils.file;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

public class MFileWrite{
        private String fileName;
        private BufferedWriter bw;

        public MFileWrite(String fileName){
            this.fileName = fileName;
            this.initFile();
        }

        private void initFile(){
            try {
                File fout = new File(this.fileName);
                FileOutputStream fos = new FileOutputStream(fout,true);
                this.bw = new BufferedWriter(new OutputStreamWriter(fos));
            } catch (Exception ex) {
                System.out.println(ex.toString());
            }
        }

        public void writeln(String s){
            try {
                this.bw.write(s);
                this.bw.newLine();
                this.bw.flush();
            } catch (Exception ex) {
                System.out.println(ex.toString());
            }
        }

        @Override
        public void finalize(){
            try {
                this.bw.flush();
                this.bw.close();
            } catch (Exception ex) {
                System.out.println(ex.toString());
            }
        }
}