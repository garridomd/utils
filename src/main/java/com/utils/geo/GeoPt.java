package com.utils.geo;

import com.utils.data.Mson;

public class GeoPt {
    private double latitude;
    private double longitude;

    public GeoPt() {
    }

    public GeoPt(double latitude, double longitude) {
        setLatitude(latitude);
        setLongitude(longitude);
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        if (latitude < -90.0D || latitude > 90.0D)
            throw new IllegalArgumentException("Latitude must be between -90 and 90 (inclusive).");
        else
            this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        if (longitude < -180.0D || longitude > 180.0D)
            throw new IllegalArgumentException("Longitude must be between -180 and 180 (inclusive).");
        else
            this.longitude = longitude;
    }

    @Override
    public String toString() {
        return Mson.toJson(this);
    }
}