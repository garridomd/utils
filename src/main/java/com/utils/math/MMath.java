package com.utils.math;

import com.utils.geo.GeoPt;

public class MMath {
    public static final double EARTH_RADIUS_METERS = 6371010.0D;

    public static double[] linspaceExp(double start, double stop, int n){
        double[] list = new double[n];
        double phi = 1.618033988749895;
        double a = start;
        double b = Math.log(stop/start)/((n-1)*Math.log(phi));
        for (int i = 0; i < n; i++){
            list[i] = Math.round(a * Math.pow(phi, b * i));
            int length = (int) (Math.log10(list[i])+1);
            if (length > 2){
                list[i] = (Math.round(list[i]/Math.pow(10, length-2)) * Math.pow(10, length-2));
            }
        }
        return list;
    }

    public static GeoPt geoPtGenerator(GeoPt geoPt, double minDistance, double maxDistance){
        double l = Math.random()*(maxDistance-minDistance)+minDistance;
        double alpha = Math.random()*Math.PI/2;

        double sx = (Math.random() < 0.5)?l*Math.cos(alpha):-l*Math.cos(alpha);
        double sy = (Math.random() < 0.5)?l*Math.sin(alpha):-l*Math.sin(alpha);

        double deltaLat = sy/EARTH_RADIUS_METERS*180/Math.PI;
        double lat = geoPt.getLatitude() + deltaLat;
        double deltaLong = sx/EARTH_RADIUS_METERS/Math.cos(lat*Math.PI/180)*180/Math.PI;
        return new GeoPt(lat, geoPt.getLongitude()+deltaLong);
    }
}