package com.utils.net;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

public class MStdHttp {
    public static String executeHTTPGet(String petition) {
        return executeHTTPGet(petition, 30000);
    }

    public static String executeHTTPPost(String petition){
        return executeHTTPPost(petition, 30000);
    }

    public static String executeHTTPPost(String petition, String data){
        return executeHTTPPost(petition, data,30000);
    }

    public static String executeHTTPPut(String petition){
        return executeHTTPPut(petition, 30000);
    }

    public static String executeHTTPPut(String petition, String data){
        return executeHTTPPut(petition, data, 30000);
    }

    public static String executeHTTPDelete(String petition){
        return executeHTTPDelete(petition, 30000);
    }

    public static String executeHTTPGet(String petition, int timeout) {
        return executeHTTPGet(petition, 30000, null);
    }

    public static String executeHTTPPost(String petition, int timeout){
        return executeHTTPPost(petition, 30000, null);
    }

    public static String executeHTTPPost(String petition, String data, int timeout){
        return executeHTTPPost(petition, data,30000, null);
    }

    public static String executeHTTPPut(String petition, int timeout){
        return executeHTTPPut(petition, 30000, null);
    }

    public static String executeHTTPPut(String petition, String data, int timeout){
        return executeHTTPPut(petition, data, 30000, null);
    }

    public static String executeHTTPDelete(String petition, int timeout){
        return executeHTTPDelete(petition, 30000, null);
    }

    public static String executeHTTPGet(String petition, int timeout, String[][] requestProperty) {
        try {
            URLConnection connection = new URL(petition).openConnection();
            if(requestProperty == null)
                connection.setRequestProperty("Accept-Charset", "UTF-8");
            else {
                for(int i = 0; i < requestProperty.length; i++){
                    connection.setRequestProperty(requestProperty[i][0], requestProperty[i][1]);
                }
            }
            connection.setConnectTimeout(timeout);
            connection.setReadTimeout(timeout);
            InputStream response = connection.getInputStream();
            Scanner scanner = new Scanner(response);
            String responseBody = scanner.useDelimiter("\\A").next();
            scanner.close();
            return responseBody;
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            return e.getLocalizedMessage();
        }
    }

    public static String executeHTTPDelete(String petition, int timeout, String[][] requestProperty){
        HttpURLConnection connection = null;
        try {
            URL url = new URL(petition);
            connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(timeout);
            connection.setReadTimeout(timeout);
            connection.setRequestMethod("DELETE");

            if(requestProperty == null)
                connection.setRequestProperty("Content-Type","application/json; charset=UTF-8");
            else {
                for(int i = 0; i < requestProperty.length; i++){
                    connection.setRequestProperty(requestProperty[i][0], requestProperty[i][1]);
                }
            }

            connection.setUseCaches(false);
            connection.setAllowUserInteraction(false);
            connection.setDoOutput(true);

            //Send request
            DataOutputStream wr = new DataOutputStream (connection.getOutputStream());
            //String parameters = "USER="+this.user+"&PWD="+this.pwd+returnUrl;
            //wr.writeBytes(urlParameters);
            wr.flush ();
            wr.close();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuffer response = new StringBuffer(); // or StringBuffer if Java version 5+
            String line;
            while ((line = rd.readLine()) != null) {
                response.append(line);
            }

            rd.close();
            String result = response.toString();
            connection.disconnect();
            return result;
        } catch (Exception e) {
            if(connection != null)
                connection.disconnect();
            return e.getLocalizedMessage();
        }
    }

    public static String executeHTTPPost(String petition, int timeout, String[][] requestProperty){
        HttpURLConnection connection = null;
        try {
            URL url = new URL(petition);
            connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(timeout);
            connection.setReadTimeout(timeout);
            connection.setRequestMethod("POST");

            if(requestProperty == null)
                connection.setRequestProperty("Content-Type","application/json; charset=UTF-8");
            else {
                for(int i = 0; i < requestProperty.length; i++){
                    connection.setRequestProperty(requestProperty[i][0], requestProperty[i][1]);
                }
            }

            connection.setUseCaches(false);
            connection.setAllowUserInteraction(false);
            connection.setDoOutput(true);

            //Send request
            DataOutputStream wr = new DataOutputStream (connection.getOutputStream());
            //String parameters = "USER="+this.user+"&PWD="+this.pwd+returnUrl;
            //wr.writeBytes(urlParameters);
            wr.flush ();
            wr.close();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuffer response = new StringBuffer(); // or StringBuffer if Java version 5+
            String line;
            while ((line = rd.readLine()) != null) {
                response.append(line);
            }

            rd.close();
            String result = response.toString();
            connection.disconnect();
            return result;
        } catch (Exception e) {
            if(connection != null)
                connection.disconnect();
            return e.getLocalizedMessage();
        }
    }

    public static String executeHTTPPut(String petition, int timeout, String[][] requestProperty){
        HttpURLConnection connection = null;
        try {
            URL url = new URL(petition);
            connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(timeout);
            connection.setReadTimeout(timeout);
            connection.setRequestMethod("PUT");

            if(requestProperty == null)
                connection.setRequestProperty("Content-Type","application/json; charset=UTF-8");
            else {
                for(int i = 0; i < requestProperty.length; i++){
                    connection.setRequestProperty(requestProperty[i][0], requestProperty[i][1]);
                }
            }

            connection.setUseCaches(false);
            connection.setAllowUserInteraction(false);
            connection.setDoOutput(true);

            //Send request
            DataOutputStream wr = new DataOutputStream (connection.getOutputStream());
            //String parameters = "USER="+this.user+"&PWD="+this.pwd+returnUrl;
            //wr.writeBytes(urlParameters);
            wr.flush ();
            wr.close();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuffer response = new StringBuffer(); // or StringBuffer if Java version 5+
            String line;
            while ((line = rd.readLine()) != null) {
                response.append(line);
            }

            rd.close();
            String result = response.toString();
            connection.disconnect();
            return result;
        } catch (Exception e) {
            if(connection != null)
                connection.disconnect();
            return e.getLocalizedMessage();
        }
    }

    public static String executeHTTPPost(String petition, String data, int timeout, String[][] requestProperty){
        HttpURLConnection connection = null;
        try {
            URL url = new URL(petition);
            connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(timeout);
            connection.setReadTimeout(timeout);
            connection.setRequestMethod("POST");

            if(requestProperty == null)
                connection.setRequestProperty("Content-Type","application/json; charset=UTF-8");
            else {
                for(int i = 0; i < requestProperty.length; i++){
                    connection.setRequestProperty(requestProperty[i][0], requestProperty[i][1]);
                }
            }

            connection.setUseCaches(false);
            connection.setAllowUserInteraction(false);
            connection.setDoOutput(true);

            //Send request
            //log.warning("Send request");
            //DataOutputStream wr = new DataOutputStream (connection.getOutputStream());
            //wr.writeBytes(data);//revisar esta conversion
            //wr.flush ();
            //wr.close();

            OutputStream os = connection.getOutputStream();
            os.write(data.getBytes("UTF-8"));
            os.flush();
            os.close();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuffer response = new StringBuffer(); // or StringBuffer if Java version 5+
            String line;

            while ((line = rd.readLine()) != null) {

                response.append(line);
            }

            rd.close();
            String result = response.toString();
            connection.disconnect();
            return result;
        } catch (Exception e) {
            if(connection != null)
                connection.disconnect();
            return e.getLocalizedMessage();
        }
    }

    public static String executeHTTPPut(String petition, String data, int timeout, String[][] requestProperty){
        HttpURLConnection connection = null;
        try {
            URL url = new URL(petition);
            connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(timeout);
            connection.setReadTimeout(timeout);
            connection.setRequestMethod("PUT");

            if(requestProperty == null)
                connection.setRequestProperty("Content-Type","application/json; charset=UTF-8");
            else {
                for(int i = 0; i < requestProperty.length; i++){
                    connection.setRequestProperty(requestProperty[i][0], requestProperty[i][1]);
                }
            }

            connection.setUseCaches(false);
            connection.setAllowUserInteraction(false);
            connection.setDoOutput(true);

            //Send request
            //DataOutputStream wr = new DataOutputStream (connection.getOutputStream());
            //wr.writeBytes(data);
            //wr.flush ();
            //wr.close();

            OutputStream os = connection.getOutputStream();
            os.write(data.getBytes("UTF-8"));
            os.flush();
            os.close();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuffer response = new StringBuffer(); // or StringBuffer if Java version 5+
            String line;

            while ((line = rd.readLine()) != null) {
                response.append(line);
            }

            rd.close();
            String result = response.toString();
            connection.disconnect();
            return result;
        } catch (Exception e) {
            if(connection != null)
                connection.disconnect();
            return e.getLocalizedMessage();
        }
    }

    public static boolean existsFile(String URLName){
        try {
            HttpURLConnection con =
                    (HttpURLConnection) new URL(URLName).openConnection();
            con.setInstanceFollowRedirects(false);
            con.setRequestMethod("HEAD");
            if((con.getResponseCode() == HttpURLConnection.HTTP_OK)){
                return true;
            }else{
                return false;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

/*
    private static String replaceOutBuffer(String line){
        line = line.replaceAll("%(?![0-9a-fA-F]{2})", "%25");
        line = line.replaceAll("\\+", "%2B");
        return line;
    }*/
}
