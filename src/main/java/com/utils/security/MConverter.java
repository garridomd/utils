package com.utils.security;

public class MConverter {
    public static String numToAlpha(long id, String alphabet){
        StringBuilder sb = new StringBuilder();
        int n;
        while (id > 0){
            n = (int)(id%alphabet.length());
            id /=alphabet.length();
            sb.append(alphabet.charAt(n));
        }
        return sb.reverse().toString().trim();
    }
    public static long alphaToNum(String id, String alphabet){
        long n = 0;
        long m;
        char[] letters = id.toCharArray();
        int a = letters.length-1;
        for (char letter : letters) {
            m = (alphabet.indexOf(letter)) * (long) Math.pow(alphabet.length(), a);
            n += m;
            a--;
        }
        return n;
    }
}
