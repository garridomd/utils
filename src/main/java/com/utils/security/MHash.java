package com.utils.security;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * This MHash class provides applications the functionality of a
 * message digest hash algorithm.
 */
public class MHash {

    /**
     * Creates a message digest with the MD5 hash algorithm.
     * @param input string for create MD5 hash.
     * @return MD5 string hash output in 32 characters.
     */
    public static String md5(String input){
        try {
            MessageDigest m;
            m = MessageDigest.getInstance("MD5");
            m.reset();
            m.update(input.getBytes());
            byte[] digest = m.digest();
            BigInteger bigInt = new BigInteger(1,digest);
            StringBuffer sb = new StringBuffer("00000000000000000000000000000000");
            sb.append(bigInt.toString(16));
            int n = sb.length();
            return sb.substring(n-32, n);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }
}
