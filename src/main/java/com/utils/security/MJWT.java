package com.utils.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.apache.commons.codec.binary.Base64;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

public class MJWT {
    private static RSAPublicKey readPublicKey(String key)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        String publicKeyPEM = key.replaceAll("-+[\\w\\s]+-+","");
        byte[] encoded = Base64.decodeBase64(publicKeyPEM);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encoded);
        return (RSAPublicKey) keyFactory.generatePublic(keySpec);
    }

    private static RSAPrivateKey readPrivateKey(String key)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        String privateKeyPEM = key.replaceAll("-+[\\w\\s]+-+", "");
        byte[] encoded = Base64.decodeBase64(privateKeyPEM);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
        return (RSAPrivateKey) keyFactory.generatePrivate(keySpec);
    }

    public static String createRSA256(
            Map<String,Object> payload,
            String publicKeyStr,
            String privateKeyStr) throws NoSuchAlgorithmException, InvalidKeySpecException {
        return createRSA256(payload, publicKeyStr, privateKeyStr, null);
    }

    public static String createRSA256(
            Map<String,Object> payload,
            String publicKeyStr,
            String privateKeyStr,
            Date expiresAt) throws NoSuchAlgorithmException, InvalidKeySpecException {
        Algorithm algorithm = Algorithm.RSA256(
                readPublicKey(publicKeyStr), readPrivateKey(privateKeyStr));
        JWTCreator.Builder jwtb = JWT.create().withPayload(payload);
        if (expiresAt != null)
            jwtb.withExpiresAt(expiresAt);
        return jwtb.sign(algorithm);
    }

    public static DecodedJWT verifyRSA256(String token, String publicKeyStr) throws NoSuchAlgorithmException, InvalidKeySpecException {
        Algorithm algorithm = Algorithm.RSA256(readPublicKey(publicKeyStr), null);
        JWT.decode(token);
        JWTVerifier verifier = JWT.require(algorithm).build();
        return verifier.verify(token);
    }

    public static String createHS256(String secret, Map<String,Object> payload){
        Algorithm algorithm = Algorithm.HMAC256(secret);
        JWTCreator.Builder jwtb = JWT.create().withPayload(payload);
        return jwtb.sign(algorithm);
    }

    public static DecodedJWT verifyHS256(String token, String secret) {
        Algorithm algorithm = Algorithm.HMAC256(secret);
        JWT.decode(token);
        JWTVerifier verifier = JWT.require(algorithm).build();
        return verifier.verify(token);
    }
}
