package com.utils.security;

import org.apache.commons.lang3.RandomStringUtils;

public class MRandom {
    public static String randomAlphanumeric(int length){
        return RandomStringUtils.randomAlphanumeric(length);
    }
}
