package com.utils.service.google;

import com.utils.net.MStdHttp;

public class GAfcm {
    private static final String URL = "https://fcm.googleapis.com/fcm/send";

    public static String sendNotification(String message, int timeout, String apiKey){
        String[][] requestProperty = {
                {"Content-Type", "application/json; charset=UTF-8"},
                {"Authorization","key="+apiKey}};
        return MStdHttp.executeHTTPPost(URL, message, timeout, requestProperty);
    }
}
