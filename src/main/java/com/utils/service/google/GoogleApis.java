package com.utils.service.google;

import com.utils.data.Mson;
import com.utils.net.MStdHttp;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

public class GoogleApis {
    private final String URL = "https://maps.googleapis.com/maps/api/geocode/json";
    private String APIKEY;

    public GoogleApis(String APIKEY) {
        this.APIKEY = APIKEY;
    }

    public String geoCode(LinkedHashMap<String,String> params) {
        StringBuffer sb = new StringBuffer(URL);
        sb.append("?key=").append(APIKEY);
        Set<String> keys = params.keySet();
        for(String k:keys){
            sb.append("&").append(k).append("=").append(params.get(k));
        }
        return MStdHttp.executeHTTPGet(sb.toString());
    }

    public GAResponse geoCodeAddress(String address) {
        LinkedHashMap<String,String> params = new LinkedHashMap<String,String>();
        params.put("address", address.replaceAll(" ", "+"));
        return Mson.fromJson(geoCode(params),GAResponse.class);
    }

    public class GAResponse {
        public String status;
        public List<Result> results;

        @Override
        public String toString() {
            return Mson.toJson(this);
        }

        public class Result {
            public List<AddressComponents> address_components;
            public Geometry geometry;
        }

        public class AddressComponents {
            public String long_name;
            public String short_name;
            public List<String> types;
        }

        public class Geometry{
            public Location location;
        }

        public class Location{
            public float lat;
            public float lng;
        }
    }
}