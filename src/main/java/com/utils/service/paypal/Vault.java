package com.utils.service.paypal;

import com.utils.net.MStdHttp;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class Vault {
    public static String getAccessToken(String username, String password, boolean live, int timeout){
        String url;
        if (live)
            url = "https://api.paypal.com/v1/oauth2/token";
        else
            url = "https://api.sandbox.paypal.com/v1/oauth2/token";
        String encoded = Base64.getEncoder().encodeToString((username+":"+password).getBytes(StandardCharsets.UTF_8));
        String[][] requestProperty = {
                {"Authorization", "Basic "+ encoded},
                {"Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"}};
        return MStdHttp.executeHTTPPost(url, "grant_type=client_credentials", timeout, requestProperty);
    }

    public static String saveCreditCard(String accessToken, String creditCard, boolean live, int timeout){
        String url;
        if (live)
            url = "https://api.paypal.com/v1/vault/credit-cards";
        else
            url = "https://api.sandbox.paypal.com/v1/vault/credit-cards";
        String[][] requestProperty = {
                {"Content-Type", "application/json; charset=UTF-8"},
                {"Authorization","Bearer "+ accessToken}};
        return MStdHttp.executeHTTPPost(url, creditCard, timeout, requestProperty);
    }

    public static String pay(String accessToken, String payment, boolean live, int timeout){
        String url;
        if (live)
            url = "https://api.paypal.com/v1/payments/payment";
        else
            url = "https://api.sandbox.paypal.com/v1/payments/payment";
        String[][] requestProperty = {
                {"Content-Type", "application/json; charset=UTF-8"},
                {"Authorization","Bearer "+ accessToken}};
        return MStdHttp.executeHTTPPost(url, payment, timeout, requestProperty);
    }
}
