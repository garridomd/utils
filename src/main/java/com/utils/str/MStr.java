package com.utils.str;

import java.text.DecimalFormat;

/**
 * This MStr class provides tools for many simple String operations.
 */
public class MStr {

    /**
     * Creates an input string completing Left with the chosen character to obtain the required length.
     * @param input string to be completed
     * @param length desired final length
     * @param character characters used to complete
     * @return string with desired length using the characters to the left
     * @throws IllegalArgumentException if input's length is less than length parameter or length
     * is less than or equal to zero.
     */
    public static String completeLeft(String input, int length, char character){
        if(length <= 0 )
            throw new IllegalArgumentException("length can not be less than or equals to zero");

        if(input.length() > length)
            throw new IllegalArgumentException("Input's length is less than length parameter");

        if(input.length() == length)
            return input;

        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < length-input.length(); i++){
            sb.append(character);
        }
        sb.append(input);
        return sb.toString();
    }


    public static boolean isEmpty(String input){
        if(input != null && !input.isEmpty())
            return false;
        return true;
    }

    public static float strToFloat(String input){
        try {
            float x = Float.parseFloat(input);
            if(!Float.isNaN(x) && !Float.isInfinite(x))
                return x;
        } catch (Exception e){
        }
        return 0.0f;
    }

    public static double strToDouble(String input){
        try {
            double x = Double.parseDouble(input);
            if(!Double.isNaN(x) && !Double.isInfinite(x))
                return x;
        } catch (Exception e){
        }
        return 0.0d;
    }

    public static int strToInt(String input){
        try {
            return Integer.parseInt(input);
        } catch (Exception e){
        }
        return 0;
    }

    public static long strToLong(String input){
        try {
            return Long.parseLong(input);
        } catch (Exception e){
        }
        return 0l;
    }

    public static String cutUTF8(String s, int limit){
        if(isEmpty(s)){
            return s;
        }
        byte[] utf8 = s.getBytes();
        if (utf8.length < limit)
            return s;

        int n16 = 0;
        int advance;
        int i = 0;
        while (i < limit) {
            advance = 1;
            if ((utf8[i] & 0x80) == 0) i += 1;
            else if ((utf8[i] & 0xE0) == 0xC0) i += 2;
            else if ((utf8[i] & 0xF0) == 0xE0) i += 3;
            else { i += 4; advance = 2; }
            if (i <= limit) n16 += advance;
        }
        return s.substring(0,n16);
    }

    public static String shortCurrencyFormat(double price){
        price = Math.abs(price);
        DecimalFormat df = new DecimalFormat("###,###,###,###,##0.##");
        if(price < 1e3)
            return df.format(price);
        if(price < 1e6)
            return df.format(price/1e3) +"K";
        if(price < 1e9)
            return df.format(price/1e6) +"M";
        if(price < 1e12)
            return df.format(price/1e9) +"B";
        if(price < 1e15)
            return df.format(price/1e12) +"T";
        if(price < 1e18)
            return df.format(price/1e15) +"Qd";
        else
            return df.format(price/1e18) +"Qn";
    }

    public static String currencyFormat(double price){
        DecimalFormat df = new DecimalFormat("###,###,###,###,##0.00");
        return df.format(price);
    }

    public static String safeString(String input){
        return (input != null)? input : "";
    }
}