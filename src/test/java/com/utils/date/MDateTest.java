package com.utils.date;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Date;

public class MDateTest {
    @BeforeClass
    public static void BeforeClass() {
        System.out.println("This is executed before Class");
    }

    @Before
    public void beforeEachTest() {
        System.out.println("This is executed before each Test");
    }

    @After
    public void afterEachTest() {
        System.out.println("This is exceuted after each Test");
    }

    @Test
    public void testExceptionIntToDate(){
        Date date = MDate.intToDate(99991231);
        System.out.println(date);
        System.out.println(MDate.dateToString(date,"yyyyMMdd"));
    }

    @Test
    public void testMXDate(){
        System.out.println(MDate.nowIntMX());
        System.out.println(MDate.nowMX());
    }
}
