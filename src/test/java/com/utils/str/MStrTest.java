package com.utils.str;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class MStrTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testCompleteLeft(){
        Assert.assertEquals("00000001",MStr.completeLeft("1", 8, '0'));
        Assert.assertEquals("111", MStr.completeLeft("111",3,'0'));
    }

    @Test
    public void testCompleteLeftExceptionZero(){
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("zero");
        MStr.completeLeft("1",0,'0');
    }

    @Test
    public void testCompleteLeftExceptionFailLength(){
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("length");
        MStr.completeLeft("111",1,'0');
    }

    @Test
    public void testCurrencyFormat(){
        Assert.assertEquals("5,127,734.48", MStr.currencyFormat(5127734.476));
        Assert.assertEquals("1,000.00", MStr.currencyFormat(1000));
    }
}
